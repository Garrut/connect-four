﻿using CFour.Logic;
using System;
using System.Drawing;
using System.Threading;

namespace CFour
{
    class Program
    {
        /// <summary>
        /// Streak necessary before a winner's declared
        /// </summary>
        const int STREAKLENGTH = 4;

        static void Main(string[] args)
        {
            Console.BufferHeight = Console.WindowHeight;

            while (true)
            {
                Game game = new Game(State.RED, STREAKLENGTH);
                var winner = State.EMPTY;

                while (winner == State.EMPTY && !game.BoardIsFull)
                {
                    Draw(game);
                    winner = game.Run(HandleInput());
                    Draw(game);
                }

                if (winner == State.RED)
                {
                    Write("RED has won!\n", ConsoleColor.Red);
                }

                if (winner == State.YELLOW)
                {
                    Write("Yellow has won!\n", ConsoleColor.Yellow);
                }

                if(winner == State.EMPTY)
                {
                    Write("Nobody has won!\n");
                }

                Write("Want to play again? (1-6) Yes, (7) Exit");
                if (HandleInput() == 6)
                {
                    break;
                }
            }

            Console.Clear();
            Write("Thanks for playing!");
            Thread.Sleep(2000);
        }

        static void Draw(Game game)
        {
            Console.Clear();
            Console.WriteLine("Turn {0} - {1}\n", game.CurrentTurn, game.CurrentPlayerString);

            Write("  ");
            for (int i = 1; i <= Game.COLUMNS; i++)
            {
                Write($"   {i} ", ConsoleColor.DarkGreen);
            }
            Write("  \n");

            for (int i = 0; i < Game.ROWS; i++)
            {
                Console.Write("|  ");
                for (int j = 0; j < Game.COLUMNS; j++)
                {
                    switch (game.GetBoardSlotState(i, j))
                    {
                        case State.RED:
                            Write("  R  ", ConsoleColor.Red);
                            break;
                        case State.YELLOW:
                            Write("  Y  ", ConsoleColor.Yellow);
                            break;
                        case State.EMPTY:
                            Write("  O  ", ConsoleColor.White);
                            break;
                        default:
                            break;
                    }
                }
                Console.Write("  |\n\n");
            }
        }

        static void Write(string s, ConsoleColor c = ConsoleColor.White)
        {
            Console.ForegroundColor = c;
            Console.Write(s);
            Console.ResetColor();
        }

        static int HandleInput() =>
                    Console.ReadKey().Key switch
                    {
                        // 1-6 Controls
                        ConsoleKey.D1 => 0,
                        ConsoleKey.D2 => 1,
                        ConsoleKey.D3 => 2,
                        ConsoleKey.D4 => 3,
                        ConsoleKey.D5 => 4,
                        ConsoleKey.D6 => 5,
                        ConsoleKey.D7 => 6,

                        // Numpad controls
                        ConsoleKey.NumPad1 => 0,
                        ConsoleKey.NumPad2 => 1,
                        ConsoleKey.NumPad3 => 2,
                        ConsoleKey.NumPad4 => 3,
                        ConsoleKey.NumPad5 => 4,
                        ConsoleKey.NumPad6 => 5,
                        ConsoleKey.NumPad7 => 6,
                        _ => HandleInput(),
                    };

    }
}
