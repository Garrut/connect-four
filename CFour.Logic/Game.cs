﻿using System.Data;

namespace CFour.Logic
{
    public enum State
    {
        EMPTY = 0,
        YELLOW = 1,
        RED = 2,
    }

    public class Game
    {
        public const int ROWS = 6;
        public const int COLUMNS = 7;

        public Game(State startingPlayer, int streakLength)
        {
            Board = new Board(ROWS, COLUMNS);
            CurrentPlayer = startingPlayer;
            CurrentTurn = 1;

            // Ensure it's always greater than 0
            StreakLength = streakLength > 0 ? streakLength : 4;
        }

        private Board Board { get; set; }

        public int StreakLength { get; }
        public int CurrentTurn { get; private set; }
        public State CurrentPlayer { get; private set; }
        public bool BoardIsFull => CurrentTurn > ROWS * COLUMNS;
        public string CurrentPlayerString => CurrentPlayer switch
        {
            State.RED => "RED",
            State.YELLOW => "YELLOW",
            _ => "UNDEFINED",
        };

        /// <summary>
        /// Checks for a winner
        /// </summary>
        /// <returns></returns>
        private State CheckForWinner()
        {
            // There can only be a winner after streaklength * 2 - 1
            return CurrentTurn >= ((StreakLength * 2) - 1) ? Board.CheckForStreak(StreakLength) : State.EMPTY;
        }

        /// <summary>
        /// Advances the game by one turn
        /// </summary>
        private void NextTurn()
        {
            if (CurrentPlayer == State.RED)
                CurrentPlayer = State.YELLOW;
            else
                CurrentPlayer = State.RED;

            CurrentTurn++;
        }

        /// <summary>
        /// Retrieves a state from a specified slot
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        public State GetBoardSlotState(int row, int column)
        {
            return Board.GetSlotState(row, column);
        }

        /// <summary>
        /// Runs an instance of the game once
        /// </summary>
        /// <param name="inputColumn">Column to drop a disc into</param>
        /// <returns>State.RED, State.YELLOW or State.Empty for no winner</returns>
        public State Run(int inputColumn)
        {
            var winner = State.EMPTY;

            // Drop a disc into the board at index of bottom row
            if (Board.DropDisc(ROWS - 1, inputColumn, CurrentPlayer))
            {
                // If a winner is found we don't want to go to the next turn
                winner = CheckForWinner();
                if (winner == State.EMPTY)
                {
                    NextTurn();
                }
            };

            return winner;
        }
    }
}
