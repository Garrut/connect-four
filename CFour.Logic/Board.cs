﻿using System.Resources;

namespace CFour.Logic
{
    public class Board
    {
        /// <summary>
        /// Create a new board instance with a specific amoun of rows and columns
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        public Board(int rows, int columns)
        {
            Slots = new State[rows, columns];
        }

        private State[,] Slots { get; set; }
        private void SetSlotState(int row, int col, State state) => Slots[row, col] = state;
        public State GetSlotState(int row, int col) => Slots[row, col];

        /// <summary>
        /// Drops a disc into the board recursively at the specified column.
        /// </summary>
        /// <param name="row">Recursive parameter.</param>
        /// <param name="col">Column to drop disc into.</param>
        /// <param name="state">State to change empty slot to.</param>
        /// <returns></returns>
        public bool DropDisc(int row, int col, State state)
        {
            if (GetSlotState(row, col) != State.EMPTY && row > 0)
            {
                return DropDisc(row - 1, col, state);
            }
            else if (GetSlotState(row, col) == State.EMPTY)
            {
                SetSlotState(row, col, state);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks in multiple directions for a streak of a certain length
        /// </summary>
        /// <param name="streakLength">Required length for streak</param>
        /// <returns>When no streak is found for a player, returns state.empty</returns>
        public State CheckForStreak(int streakLength)
        {
            for (int row = Game.ROWS - 1; row >= 0; row--)
            {
                for (int column = Game.COLUMNS - 1; column >= 0; column--)
                {
                    var slot = GetSlotState(row, column);
                    var length = streakLength - 1;

                    // If slot is empty or we go out of bounds then no checking is needed
                    if (slot != State.EMPTY)
                    {
                        // Horizontal checking
                        if (column - length >= 0)
                        {
                            for (int i = length; i >= 0; i--)
                            {
                                if (GetSlotState(row, column - i) != slot)
                                {
                                    break;
                                }
                                else if (i == 0)
                                {
                                    return slot;
                                }
                            }
                        }

                        // Vertical checking
                        if (row - length >= 0)
                        {
                            for (int i = length; i >= 0; i--)
                            {
                                if (GetSlotState(row - i, column) != slot)
                                {
                                    break;
                                }
                                else if (i == 0)
                                {
                                    return slot;
                                }
                            }
                        }

                        // Diagonal checking BottomRight -> TopLeft
                        if (row - length >= 0 && column - length >= 0)
                        {
                            for (int i = length; i >= 0; i--)
                            {
                                if (GetSlotState(row - i, column - i) != slot)
                                {
                                    break;
                                }
                                else if (i == 0)
                                {
                                    return slot;
                                }
                            }
                        }

                        // Diagonal checking BottomLeft -> UpperRight
                        if (row - length >= 0 && column + length < Game.COLUMNS)
                        {
                            for (int i = length; i >= 0; i--)
                            {
                                if (GetSlotState(row - i, column + i) != slot)
                                {
                                    break;
                                }
                                else if (i == 0)
                                {
                                    return slot;
                                }
                            }
                        }
                    }
                }
            }

            return State.EMPTY;
        }
    }
}
